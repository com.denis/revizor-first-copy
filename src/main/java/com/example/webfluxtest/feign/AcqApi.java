package com.example.webfluxtest.feign;

import com.example.webfluxtest.model.Greeting;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "revizor-acqapi-serverice")
public interface AcqApi {


  @GetMapping(value = "/hello/{accPay}",produces = {"*/*"})
  ResponseEntity<Greeting> getUsingGET3(@Parameter(in = ParameterIn.PATH, description = "accPay", required = true, schema = @Schema()) @PathVariable("accPay") String accPay);

}

