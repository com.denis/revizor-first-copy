package com.example.webfluxtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class WebFluxTestApplication {

  public static void main(String[] args) {

    SpringApplication.run(WebFluxTestApplication.class, args);
  }

}
