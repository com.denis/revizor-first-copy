package com.example.webfluxtest.domain;

import com.example.webfluxtest.service.SchedulingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

  @Autowired
  private SchedulingService schedulingService;

  @GetMapping(value = "hehe")
  public void test() {
    schedulingService.getFeignClient();
  }
}
