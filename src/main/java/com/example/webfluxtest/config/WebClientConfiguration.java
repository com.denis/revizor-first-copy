package com.example.webfluxtest.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.jackson.JacksonEncoder;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.context.annotation.Bean;
import feign.jackson.JacksonDecoder;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebClientConfiguration {

  @Bean
  Encoder encoder(ObjectMapper objectMapper) {
    return new JacksonEncoder(objectMapper);
  }

  @Bean
  Decoder decoder(ObjectMapper objectMapper) {
    return new ResponseEntityDecoder(new JacksonDecoder(objectMapper));
  }
}