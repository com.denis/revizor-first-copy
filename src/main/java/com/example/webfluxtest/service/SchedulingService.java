package com.example.webfluxtest.service;

import com.example.webfluxtest.feign.AcqApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SchedulingService {

  @Autowired
  private AcqApi acqApi;

  public void getFeignClient() {
   var entity =  acqApi.getUsingGET3("1232");
   if (entity.hasBody())
   {
     System.out.println("haha");
   }
   else {
     System.out.println("hehe");
   }
  }

  @Scheduled(cron = "0 42 11 ? * *")
  public void getblalbla() {
    System.out.println("HEHEHEHE");
  }

}
